package revidd.Storefront;

import org.testng.annotations.Test;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import revidd.Storefront.*;

public class LoginTest {
    static WebDriver driver;
    
	Credential_validation_functionality cvf ;
	@Test(description = "Login testing with correct credential")
	public void login() {

		//System.setProperty("webdriver.chrome.driver", "/home/mohit/Downloads/chromedriver");
	      WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--window-size=1400,600");

		options.addArguments("--headless");
		driver = new ChromeDriver(options);

		// WebDriver driver = new ChromeDriver();

		driver.get("https://revidd123.dev.revidd.tv/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
		 cvf = new Credential_validation_functionality(driver);

		cvf.signinbutton().click();
		
		cvf.emailid().sendKeys("rosh30@gmail.com");
		cvf.click_continue().click();
		cvf.password().sendKeys("snoopy");
		cvf.submit().click();
		driver.close();

	}

	@Test(description = "email id error message testing with incorrect credential")
	public void loginfailedatwrongmail() {

		//System.setProperty("webdriver.chrome.driver", "/home/mohit/Downloads/chromedriver");
        WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--window-size=1400,600");
		options.addArguments("--headless");
		driver = new ChromeDriver(options);

		// WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.get("https://revidd123.dev.revidd.tv/");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		Credential_validation_functionality cvf = new Credential_validation_functionality(driver);

		cvf.signinbutton().click();

		cvf.emailid().sendKeys("gfdhdgh@gmail.com");
		cvf.click_continue().click();
		String ActualMsg = cvf.error_messages().getText();

		String errorMsg = "Could not find an account with this email.";

		if (ActualMsg.equals(errorMsg)) {
			System.out.println("Test Case Passed");
		} else {
			System.out.println("Test Case Failed");
		}
		;

		driver.close();
	}

}
