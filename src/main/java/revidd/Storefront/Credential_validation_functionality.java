package revidd.Storefront;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Credential_validation_functionality {
	public WebDriver driver;

	public Credential_validation_functionality(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//a[@href='/content/live_event_1?section=All%20Live%20events']")
	WebElement view;
	@FindBy(xpath = "//a[@href='/signin']/button")
	WebElement signin;
	@FindBy(id = "signInEmail")
	WebElement user;
	@FindBy(xpath = "//button[@type='submit']")
	WebElement continueclick;
	@FindBy(xpath = "//input[@id='signInPassword']")
	WebElement pswd;
	@FindBy(xpath = "//button[@type='submit']")
	WebElement submit;
	@FindBy(xpath = "//p[@class='text-14 text-warningText']")
	WebElement errormess;
	@FindBy(xpath = "//img[@alt='Ravi']")
	WebElement Ravi;

	public WebElement signinbutton() {
		return signin;
	}

	public WebElement emailid() {
		return user;
	}

	public WebElement click_continue() {
		return continueclick;
	}

	public WebElement password() {
		return pswd;
	}

	public WebElement submit() {
		return submit;
	}

	public WebElement error_messages() {
		return errormess;
	}

	public WebElement Click_Ravi() {
		return Ravi;
	}

	public WebElement Click_view_more() {
		return view;
	}

}